# Makefile for the shipper project

VERS=$(shell sed <shipper -n -e '/^shipper_version *= *"\(.*\)"/s//\1/p')

prefix?=/usr/local
mandir?=share/man
target=$(DESTDIR)$(prefix)

DOCS    = README.adoc TODO.adoc COPYING shipper.xml
SOURCES = shipper Makefile $(DOCS) control shipper-logo.png

all: shipper-$(VERS).tar.gz

install: shipper.1
	install -d "$(target)/bin"
	install -m 755 shipper "$(target)/bin/"
	install -d "$(target)/$(mandir)/man1"
	gzip <shipper.1 >"$(target)/$(mandir)/man1/shipper.1.gz"

shipper.1: shipper.xml
	xmlto man shipper.xml
shipper.html: shipper.xml
	xmlto html-nochunks shipper.xml

EXTRA = shipper.1
shipper-$(VERS).tar.gz: $(SOURCES) $(EXTRA)
	@mkdir shipper-$(VERS)
	@cp $(SOURCES) $(EXTRA) shipper-$(VERS)
	@tar -czf shipper-$(VERS).tar.gz shipper-$(VERS)
	@rm -fr shipper-$(VERS)

shipper-$(VERS).md5: shipper-$(VERS).tar.gz
	@md5sum shipper-$(VERS).tar.gz >shipper-$(VERS).md5

clean:
	rm -f *.1 *.tar.gz *.rpm *.tar.gz *.html *.md5 *.sha*

check:
	cd test; $(MAKE) --quiet

version:
	@echo $(VERS)

pylint:
	@pylint --score=n shipper

dist: shipper-$(VERS).tar.gz shipper-$(VERS).md5 

NEWSVERSION=$(shell sed -n <NEWS.adoc '/^[0-9]/s/:.*//p' | head -1)

release: check shipper-$(VERS).tar.gz shipper-$(VERS).md5 shipper.html
	@[ $(VERS) = $(NEWSVERSION) ] || { echo "Version mismatch!"; exit 1; }
	shipper -g version=$(VERS) | sh -x -e

refresh: shipper-$(VERS).md5 shipper.html
	@[ $(VERS) = $(NEWSVERSION) ] || { echo "Version mismatch!"; exit 1; }
	shipper -N -w version=$(VERS) | sh -x -e

